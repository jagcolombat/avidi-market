import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HandlerErrorService {

  constructor() { }

  handler(err: HttpErrorResponse): Observable<any> {
    const error = err.error.error || err.error.message || err.statusText;
    return throwError(() => new Error(error));
  }
}
