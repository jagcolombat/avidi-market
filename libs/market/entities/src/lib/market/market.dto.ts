export interface MarketDto {
    id: number;
    symbol?: string;
    name: string;
    industry: string;
    sector?: string;
    country?: string;
    ipoYear?: number;
    marketCap?: number;
    volume?: number;
    netChange?: number;
    netChangePercent?: number;
    lastPrice: number;
    createdAt?: string;
    updatedAt?: string;
}