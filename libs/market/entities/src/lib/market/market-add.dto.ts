export interface MarketAddDto {    
    name: string;
    symbol: string;
    industry: string;
    sector: string;
    country: string;
    ipoYear?: number;
    marketCap?: number;
    volume?: number;
    netChange?: number;
    netChangePercent?: number;
    lastPrice: number;
}