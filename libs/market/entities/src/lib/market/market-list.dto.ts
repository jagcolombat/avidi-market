import { MarketDto } from './market.dto';

export interface MarketListDto {
  markets: MarketDto[];
}