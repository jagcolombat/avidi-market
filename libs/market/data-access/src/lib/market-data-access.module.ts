import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketDataAccessService } from './services/entities/market-data-access.service';
import { MarketApiRestService } from './services/market-api-rest.service';
import { SharedApiRestModule } from '@avidi/shared/api-rest';

@NgModule({
  imports: [CommonModule, SharedApiRestModule],
  providers: [MarketDataAccessService, MarketApiRestService]
})
export class MarketDataAccessModule {}
