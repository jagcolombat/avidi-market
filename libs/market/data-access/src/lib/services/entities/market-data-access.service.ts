import { Injectable } from '@angular/core';
import { MarketAddDto, MarketDto, MarketListDto } from '@avidi/market/entities';
import { Observable } from 'rxjs';
import { MarketApiRestService } from '../market-api-rest.service';

@Injectable()
export class MarketDataAccessService {
  selectedMarket?: MarketDto;

  constructor(private restService: MarketApiRestService) { }

  getMarketById(id: string): Observable<MarketDto> {
    return this.restService.get<MarketDto>('/markets/' + id);
  }

  getMarkets(): Observable<MarketListDto> {
    return this.restService.get<MarketListDto>('/markets/');
  }

  saveMarket(market: MarketAddDto): Observable<any> {
    return this.restService.post<any, any>('/markets/', market);
  }

  updateMarket(market: MarketDto): Observable<any> {
    return this.restService.put<any, any>('/markets/' + market.id, market);
  }

  deleteMarket(id: string): Observable<any> {
    return this.restService.delete<any>('/markets/' + id);
  }

  getSelectedMarket() {
    return this.selectedMarket;
  }
}
