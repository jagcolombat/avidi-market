export interface AgGridDef {
  field: string,
  title: string,
  width?: number,
  minWidth?: number,
  type?: string,
  sortable?: boolean,
  cellRenderer?: any;
  cellRendererFramework?: any,
  cellEditorFramework?: any,
  cellRendererParams?: any,
  checkboxSelection?: boolean,
  editable?: boolean
}
