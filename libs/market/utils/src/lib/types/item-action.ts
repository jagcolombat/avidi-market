import { ItemActionEnum } from '../enum/item-action.enum';
import { MarketDto } from '@avidi/market/entities';

export interface ItemAction {
  action: ItemActionEnum,
  item: MarketDto
}
