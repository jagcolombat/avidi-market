import { Component, EventEmitter, HostListener, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { GridApi, GridOptions, Module } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { CustomHeaderComponent } from './custom-header.component';
import { AgGridDef } from '@avidi/market/utils';

@Component({
  selector: 'avidi-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.scss']
})
export class AgGridComponent implements OnChanges, OnInit, OnDestroy {
  @Output() updateData = new EventEmitter<boolean>();
  @Output() evSelItem = new EventEmitter<any>();
  @Input() columns?: AgGridDef[];
  @Input() data: any;
  @Input() rowSelection: string = 'multiple';
  @Input() checkboxSelection: boolean = true;
  @Input() headerHeight = 48;
  @Input() footer?: boolean;
  @Input() bottomData?: Array<any>;
  @Input() frameworkComponents: any;
  @Input() context: any;
  @Input() pagination: boolean = false;
  @Input() paginationPageSize: number = 10;
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.gridOptions?.api?.sizeColumnsToFit();
  }

  columnDefs: any;
  gridReady?: boolean;
  public gridOptions: GridOptions = <GridOptions>{rowData: []};
  private gridApi?: GridApi;
  private subscriptions: Subscription[] = [];

  constructor() {
    this.context = { componentParent: this };
  }

  ngOnChanges(sc: SimpleChanges) {
    console.log('change', sc);
    if (sc['data'] && this.gridReady) this.updateItems();
  }

  ngOnInit() {
    this.updateGridOptions();
    /*if(this.footer) {
      this.setTotal();
    }*/
  }

  onGridReady(params: any) {
    console.log('on grid ready');
    this.gridReady = true;
    this.gridApi = params.api;
    if (this.data) this.updateItems(this.data);
  }

  private createColumnDefs() {
    this.columnDefs = [{
      headerCheckboxSelection: false,
      checkboxSelection: this.checkboxSelection,
      flex: 1
    }];

    this.columns?.map((v, i, arr) => {
      this.columnDefs[i] = {
        ...this.columnDefs[i],
        displayName: v.title,
        field: v.field,
        type: v.type,
        width: v.width,
        cellEditorFramework: v.cellEditorFramework,
        cellRendererFramework: v.cellRendererFramework,
        cellRenderer: v.cellRenderer,
        cellRendererParams: v.cellRendererParams,
        sortable: v.sortable,
        minWidth: v.minWidth,
        editable: v.editable
      }
    });
  }

  updateGridOptions() {
    //console.log('update grid options');
    this.gridOptions = <GridOptions>{
      rowData: [],
      onGridReady: () => {
        //console.log('grid ready on update grid options');
        this.gridOptions?.api?.sizeColumnsToFit();
      },
      onFirstDataRendered: () => {
        this.gridOptions?.api?.sizeColumnsToFit();
      },
      onRowSelected: (ev) => {
        //console.log('onRowSelected', ev);
        this.evSelItem.emit(this.gridOptions?.api?.getSelectedRows());
      },
      onRowClicked: (ev) => {
        //console.log('onRowClicked', ev);
        this.evSelItem.emit(this.gridOptions?.api?.getSelectedRows());
      },
      onBodyScroll: (ev) => {
        // Conflicts on small viewports
        //this.gridOptions.api.sizeColumnsToFit();
      },
      onGridColumnsChanged: () => {
        this.gridOptions?.api?.sizeColumnsToFit();
      },
      onViewportChanged: () => {
        this.gridOptions?.api?.sizeColumnsToFit();
      },
      rowHeight: 60,
      minColWidth: 100,
    };
    this.createColumnDefs();
  }

  selectOrDeselectAll() {
    if (this.checkboxSelection) {
      if (this.gridOptions?.api?.getSelectedNodes().length !== this.gridOptions?.api?.getDisplayedRowCount()) {
        this.gridOptions?.api?.selectAll();
      } else {
        this.gridOptions?.api?.deselectAll();
      }
    }
  }

  onAddRow(data: any) {
    this.gridOptions?.api?.applyTransaction({ add: [data] });
  }

  onDeleteRow(data: any) {
    this.gridOptions?.api?.deselectAll();

    this.gridOptions?.api?.applyTransaction({ remove: [data] });
  }

  onUpdateRow(data: any) {
    this.gridOptions?.api?.deselectAll();

    this.gridOptions?.api?.forEachNode(function (node) {
      if (node.data.id === data.id) {
        node.setData(data);
      }
    });
  }

  getRowData() {
    const rowData: any[] = [];
    this.gridOptions?.api?.forEachNode(function (node) {
      rowData.push(node.data);
    });
    //console.log('Row Data:', rowData);
    return rowData;
  }

  clearData() {
    this.gridOptions?.api?.setRowData([]);
  }

  updateItems(arrPO?: any[]) {
    this.clearData();
    this.data.map((data: any) => {
      //console.log('updateItems', data);
      this.onAddRow(data);
    });
  }

  setTotal(total?: number) {
    this.bottomData = [{
      name: 'Total',
      description: undefined,
      price: undefined,
      quantity: total ? total.toFixed(2) : 0,
    }];
  }

  ngOnDestroy() {
    this.subscriptions.map(sub => sub.unsubscribe());
  }
}
