import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SharedMaterialModule } from '@avidi/shared/material';
import { RouterModule } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { AgGridComponent } from './components/ag-grid/ag-grid.component';
@NgModule({
  imports: [
    CommonModule,
    SharedMaterialModule,
    RouterModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    AgGridComponent
  ],
  exports: [
    SharedMaterialModule,
    HeaderComponent,
    FooterComponent,
    AgGridComponent
  ]
})
export class MarketUiModule {}
