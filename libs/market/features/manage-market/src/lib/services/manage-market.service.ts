import { Injectable } from '@angular/core';
import { MarketDataAccessService } from '@avidi/market/data-access';
import { MarketAddDto, MarketDto } from '@avidi/market/entities';
import { Observable } from 'rxjs';

@Injectable()
export class ManageMarketService {
  
  constructor(private marketDAServ: MarketDataAccessService) { }

  save(market: MarketAddDto): Observable<any> {
    return this.marketDAServ.saveMarket(market)
  }

  update(market: MarketDto): Observable<any> {
    return this.marketDAServ.updateMarket(market);
  }

  marketSelected(): MarketDto | undefined {
    // console.log('marketSelected', this.marketDAServ.selectedMarket)
    return this.marketDAServ.selectedMarket;
  }

  cleanSelection() {
    this.marketDAServ.selectedMarket = undefined;
  }
}
