import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ManageMarketComponent } from './components/manage-market/manage-market.component';
import { MarketUiModule } from '@avidi/market/ui';
import { ReactiveFormsModule } from '@angular/forms';
import { ManageMarketService } from './services/manage-market.service';
import { MarketDataAccessModule } from '@avidi/market/data-access';

@NgModule({
  imports: [
    CommonModule,
    MarketUiModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {path: '', pathMatch: 'full', component: ManageMarketComponent}
    ]),
  ],
  declarations: [
    ManageMarketComponent
  ],
  providers: [ManageMarketService]
})
export class MarketFeaturesManageMarketModule {}
