import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MarketAddDto, MarketDto } from '@avidi/market/entities';
import { ManageMarketService } from '../../services/manage-market.service';

const marketDtoEmpty = { 
  id: 0, 
  name: '', 
  sector: '', 
  industry: '', 
  country: '', 
  symbol: '',  
  lastPrice: 0,
  marketCap: 0,
  volume: 0,
  ipoYear: 0,
  netChange: 0,
  netChangePercent: 0,
}

@Component({
  selector: 'avidi-manage-market',
  templateUrl: './manage-market.component.html',
  styleUrls: ['./manage-market.component.scss']
})
export class ManageMarketComponent implements OnInit {

  @ViewChild('formDirective') private formDirective: NgForm | undefined;
  @Output() evAddMarket = new EventEmitter<MarketAddDto>();
  @Output() evUpdateMarket = new EventEmitter<MarketDto>();
  @Input() market2Edit: MarketDto = marketDtoEmpty;
  @Input() error = '';
  title = 'Add';
  action = 'Save';

  form: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router,
    private mngMarketServ: ManageMarketService) {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      symbol: ['', Validators.required],
      sector: ['', Validators.required],
      industry: ['', Validators.required],
      country: ['', Validators.required],
      lastPrice: ['', Validators.required],
      ipoYear: ['', Validators.required],
      marketCap: ['', Validators.required],
      volume: ['', Validators.required],
      netChange: ['', Validators.required],
      netChangePercent: ['', Validators.required],
      description: ['', Validators.maxLength(250)]
    });

    if(this.mngMarketServ.marketSelected() !== undefined) {
      this.title = 'Edit';
      this.action = 'Update';
      this.market2Edit = <MarketDto> this.mngMarketServ.marketSelected();
      this.setSelMarket();
      this.mngMarketServ.cleanSelection();
    } else {
      this.market2Edit = marketDtoEmpty;
    }
  }

  ngOnChanges(sc: SimpleChanges){
    //console.log('changes', sc);
    if(sc['market2Edit']) {
      this.setSelMarket();
    }
    if(sc['error']) {
      //console.error(sc['error']);
      //if(sc['error']) this.formDirective.resetForm();
    }
  }

  setSelMarket() {
    this.form.patchValue({
          name: this.market2Edit?.name,
          symbol: this.market2Edit?.symbol,
          industry: this.market2Edit?.industry,
          sector: this.market2Edit?.sector,
          country: this.market2Edit?.country,
          lastPrice: this.market2Edit?.lastPrice,
          ipoYear: this.market2Edit?.ipoYear,
          marketCap: this.market2Edit?.marketCap,
          volume: this.market2Edit?.volume,
          netChange: this.market2Edit?.netChange,
          netChangePercent: this.market2Edit?.netChangePercent
        },
      )
  }

  ngOnInit(): void {
    //console.log('ngOnInit', this.market2Edit);
  }

  get f() { return this.form.controls; }

  fc(controlName: string) { return this.form.controls[controlName]; }

  onSubmit() {
    //console.log('form', this.f);
    this.submitted = true;
    if(this.form.valid){
      //console.log(this.market2Edit)
      this.market2Edit && this.market2Edit.id !== 0 ? this.updateMarket() : this.saveMarket();
    }
  }

  saveMarket(){
    this.mngMarketServ.save({
      name: this.f['name'].value,
      symbol: this.f['symbol'].value,
      industry: this.f['industry'].value,
      sector: this.f['sector'].value,
      country: this.f['country'].value,
      lastPrice: this.f['lastPrice'].value,
      ipoYear: this.f['ipoYear'].value,
      marketCap: this.f['marketCap'].value,
      volume: this.f['volume'].value,
      netChange: this.f['netChange'].value,
      netChangePercent: this.f['netChangePercent'].value      
    }).subscribe({
      next: (v) => {
        console.log('added market', v);
        this.router.navigateByUrl('/markets');
      },
      error: (e) => console.error(e),
      complete: () => this.formDirective?.resetForm()
    });
  }

  updateMarket(){
    this.mngMarketServ.update({
      id: this.market2Edit.id,
      name: this.f['name'].value,
      symbol: this.f['symbol'].value,
      industry: this.f['industry'].value,
      sector: this.f['sector'].value,
      country: this.f['country'].value,
      lastPrice: this.f['lastPrice'].value,
      ipoYear: this.f['ipoYear'].value,
      marketCap: this.f['marketCap'].value,
      volume: this.f['volume'].value,
      netChange: this.f['netChange'].value,
      netChangePercent: this.f['netChangePercent'].value
    }).subscribe({
      next: (v) => {
        console.log('updated market', v);
        this.router.navigateByUrl('/markets');
      },
      error: (e) => console.error(e),
      complete: () => {
        this.resetMarketEdit();
        this.formDirective?.resetForm();
      }
    });
  }

  resetMarketEdit(){
    this.market2Edit = marketDtoEmpty;
  }

}
