import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DisplayMarketComponent } from './components/display-market/display-market.component';
import { MarketUiModule } from '@avidi/market/ui';
import { DisplayMarketsService } from './services/display-markets.service';

@NgModule({
  imports: [
    CommonModule,
    MarketUiModule,
    RouterModule.forChild([
      {path: '', pathMatch: 'full', component: DisplayMarketComponent}
    ]),
  ],
  declarations: [
    DisplayMarketComponent
  ],
  providers: [DisplayMarketsService]
})
export class MarketFeaturesDisplayMarketsModule {}
