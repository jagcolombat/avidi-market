import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarketDto, MarketListDto } from '@avidi/market/entities';
import { DisplayMarketsService } from '../../services/display-markets.service';

@Component({
  selector: 'avidi-display-market',
  templateUrl: './display-market.component.html',
  styleUrls: ['./display-market.component.scss']
})
export class DisplayMarketComponent implements OnInit {
  columnDefs = [
    {
      field: "name",
      title: "Name",      
      minWidth: 220,
      sortable: true
    },
    {
      field: "sector",
      title: "Sector",    
      minWidth: 150, 
      sortable: true
    },
    {
      field: "industry",
      title: "Industry",    
      minWidth: 150, 
      sortable: true
    },
    {
      field: "country",
      title: "Country",    
      minWidth: 130,  
      sortable: true
    },
    {
      field: "symbol",
      title: "Symbol",
      minWidth: 120, 
      sortable: true
    },
    {
      field: "lastPrice",
      title: "Price",
      width: 150,
      sortable: true,
      cellRenderer: (params: any) => {
        return `$${new Number(params.value).toFixed(2)}`
      }
    },
    {
      field: "ipoYear",
      title: "Year",   
      minWidth: 80, 
      sortable: true
    },
    {
      field: "marketCap",
      title: "Capacity",
      flex: 1,
      sortable: true
    },
    {
      field: "netChange",
      title: "Net change",    
      width: 130,
      sortable: true
    },
    {
      field: "netChangePercent",
      title: "Percent",     
      width: 130,
      sortable: true
    }
  ]
  data: any = [];
  editBtnDisabled: boolean = true;
  delBtnDisabled: boolean = true;
  paginationPageSize= 5;
  selectedItem?: MarketDto;

  constructor(private displayMarketsServ: DisplayMarketsService,
    private router: Router) { }

  ngOnInit(): void {
    this.displayMarketsServ.getMarkets().subscribe({
      next: (v: any) => this.setData(v),
      error: (e: any) => console.error(e)
    })
  }

  setData(v: MarketListDto): void {
    console.log('markets', v);
    this.data = v;
  }

  onItemSelected($event: any) { 
    console.log($event);
    this.selectedItem = <MarketDto>$event[0];
    this.editBtnDisabled = this.delBtnDisabled = false;
  }

  editMarket(){
    if(this.selectedItem){
      this.displayMarketsServ.selectItem(<MarketDto> this.selectedItem)
      this.router.navigateByUrl('/manage');
    } else {
      console.error('We cannot edit market due to there is not market selected');
    }
    
  }

  deleteMarket(){
    this.displayMarketsServ.removeMarket(<MarketDto> this.selectedItem).subscribe({
      next: (v: any) => {
        console.log('Was removed the selected market', v);
      },
      error: (e: any) => console.error(e)
    })
  }

}
