import { Injectable } from '@angular/core';
import { MarketDataAccessService } from '@avidi/market/data-access';
import { MarketDto } from '@avidi/market/entities';
import { Observable } from 'rxjs';

@Injectable()
export class DisplayMarketsService {

  constructor(private marketDAServ: MarketDataAccessService) { }

  getMarkets(): Observable<any> {
    return this.marketDAServ.getMarkets();
  }

  removeMarket(item: MarketDto): Observable<any> {
    return this.marketDAServ.deleteMarket(item.id + '');
  }

  selectItem(item: MarketDto) {
    this.marketDAServ.selectedMarket = item;
  }

  deselectItem() {
    return this.marketDAServ.selectedMarket = undefined;
  }
}
