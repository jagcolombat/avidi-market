import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketUiModule } from '@avidi/market/ui';
import { RouterModule } from '@angular/router';

import { MainComponent } from './components/main/main.component';

@NgModule({
  imports: [
    CommonModule,
    MarketUiModule,
    RouterModule.forChild([
      {
        path: '', component: MainComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'markets'},
          {
            path: 'manage',
            loadChildren: () => import('@avidi/market/features/manage-market')
              .then(m => m.MarketFeaturesManageMarketModule)
          },
          {
            path: 'markets',
            loadChildren: ( ) => import('@avidi/market/features/display-markets')
              .then(m => m.MarketFeaturesDisplayMarketsModule)
          }
        ]
      }
    ])  
  ],
  declarations: [
    MainComponent
  ],
})
export class MarketFeaturesLayoutModule {}
