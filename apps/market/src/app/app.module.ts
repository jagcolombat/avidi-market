import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedMaterialModule } from '@avidi/shared/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { errorInterceptorProvider } from '@avidi/shared/handler-errors';
import { RouterModule } from '@angular/router';
import { MarketDataAccessModule } from '@avidi/market/data-access';

import { AppComponent } from './app.component';
import { NxWelcomeComponent } from './nx-welcome.component';
import { SharedApiRestModule } from '@avidi/shared/api-rest';

@NgModule({
  declarations: [AppComponent, NxWelcomeComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MarketDataAccessModule,
    SharedMaterialModule,
    RouterModule.forRoot([
      { path: '', loadChildren: () => import('@avidi/market/features/layout')
          .then(m => m.MarketFeaturesLayoutModule)
      },
      { path: '**', redirectTo: ''}
    ], {
      paramsInheritanceStrategy: 'always',
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      onSameUrlNavigation: 'reload',
      enableTracing: false
    })
  ],
  providers: [errorInterceptorProvider],
  bootstrap: [AppComponent],
})
export class AppModule {}